package yourfault.yourfault;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.wearable.Wearable;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


public class MapActivity extends FragmentActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    public GoogleMap myMap;
    public double currlat;
    public double currlng;
    public double listlat;
    public double listlng;
    public int distance;
    public String magnitude;
    public String earthquakeLocation;
    private static final String TAG = "______ DEBUGGING MAP: ";
    public static int UPDATE_INTERVAL_MS = 10000;
    public static int FASTEST_INTERVAL_MS = 1000;
    private int notificationId = 0;
    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_maps);
        buildAPI();

        // Add map to fragment
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void buildAPI() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        // Save map and enable location
        map.setMyLocationEnabled(true);
        myMap = map;

        // Pull current location
        try {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            currlng = mCurrentLocation.getLongitude();
            currlat = mCurrentLocation.getLatitude();
        } catch (SecurityException|NullPointerException e) {
            // Default set to Singapore
            currlng = 1.276294;
            currlat = 103.793409;
        }

        ArrayList<String> listItemSelected =  getIntent().getStringArrayListExtra("Latlng");
        try {
            listlat = Double.parseDouble(listItemSelected.get(0));
            listlng = Double.parseDouble(listItemSelected.get(1));
            magnitude = listItemSelected.get(2);
            earthquakeLocation = listItemSelected.get(3);
        } catch (NullPointerException e) {
            listlat = 0;
            listlng = 0;
        }

        ((TextView) findViewById(R.id.magnitudeTextView)).setText(magnitude + " M");
        ((TextView) findViewById(R.id.locationTextView)).setText(earthquakeLocation);
        distance = (int) Math.round(distanceCalc(listlat, listlng, currlat, currlng));
        ((TextView) findViewById(R.id.distanceTextView)).setText(Integer.toString(distance) + " km away");

        // Change colors
        RelativeLayout mText = (RelativeLayout) findViewById(R.id.mapViewtext);

        Double currMagnitude = Double.parseDouble(magnitude);
        if (currMagnitude <= 4.0) {
            mText.setBackgroundColor(0xFF2ecc71);
        } else if (currMagnitude <= 7.0 && currMagnitude > 4.0) {
            mText.setBackgroundColor(0xFFf1c40f);
        } else {
            mText.setBackgroundColor(0xFFe74c3c);
        }

        moveMap(listlat, listlng, 8);
        String stringLocation = "";
        try {
            String[] arrayLocation = earthquakeLocation.split(" ");
            for (int i = 3; i < arrayLocation.length; i++) {
                stringLocation += arrayLocation[i];
            }
        } catch (Exception e) {
            stringLocation = "Earthquake";
        }


        String notificationText = magnitude + " M\n" + Integer.toString(distance) + " km away";
        sendNotification(stringLocation, notificationText);

    }

    // Function to move the map to (lat, long) coordinates with a zoom (0 = whole earth)
    protected void moveMap(double lat, double lng, int zoom) {
        LatLng coordinate = new LatLng(lat, lng);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, zoom);
        myMap.addMarker(new MarkerOptions()
                .position(coordinate)
                .title("Marker")
                .draggable(true));
        myMap.animateCamera(yourLocation);
    }

    public static double distanceCalc (
            double lat1, double lng1, double lat2, double lng2) {
        int r = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                        * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = r * c;
        return d;
    }


    @Override
    public void onConnected(Bundle bundle) {
        // Build a request for continual location updates
        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL_MS)
                .setFastestInterval(FASTEST_INTERVAL_MS);

        // Send request for location updates
        LocationServices.FusedLocationApi
                .requestLocationUpdates(mGoogleApiClient,
                        locationRequest, this);
        mLocationRequest = locationRequest;

    }

    public void sendNotification(String title, String text) {
        Intent viewIntent = new Intent(this, MainActivity.class);

        PendingIntent viewPendingIntent = PendingIntent.getActivity(this, 0, viewIntent, 0);
        //Create an Intent for the BroadcastReceiver
        Intent buttonIntent = new Intent(this, NotificationReceiver.class);
        buttonIntent.putExtra("notif", notificationId);
        PendingIntent btPendingIntent = PendingIntent.getBroadcast(this, 0, buttonIntent, 0);
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(viewPendingIntent)
                .addAction(R.drawable.check, "Dismiss", btPendingIntent)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId++, notification);
    }


    @Override
    public void onLocationChanged(Location location) {}


    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connResult) {}

}
