package yourfault.yourfault;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;
import android.os.Handler;
import android.os.Bundle;
import android.view.Window;
import android.os.StrictMode;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.view.View;
import android.widget.ListView;
import java.util.HashMap;

import java.util.ArrayList;
import java.util.Calendar;
import java.net.URL;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;

public class MainActivity extends Activity {

    private static final HashMap<Integer, ArrayList<String>> Earthquakes = new HashMap<Integer, ArrayList<String>>();
    private ArrayList<String> infoArray;
    private int earthquakeCounter = 0;
    private String TAG = "------____---------";

    final Handler handler = new Handler();
    final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            earthquakeApiPull();
            handler.postDelayed(this, 5000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        handler.postDelayed(runnable, 0);
        startService();
    }

    // Starts the wearable listener for wear shakes
    private void startService() {
        Intent wearableService = new Intent(this, WearableService.class);
        wearableService.putExtra("Start Service", 1);
        startService(wearableService);
    }

    private void earthquakeApiPull() {
        URL url;
        String todaysDate = (new SimpleDateFormat("yyyy-MM-dd")).format(Calendar.getInstance().getTime());
        try {
            url = new URL("http://earthquake.usgs.gov/fdsnws/event/1/query?format=text&starttime="+ todaysDate);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }

        httpConnection(url);
        setEarthquakeCounter();
        setListView();
    }

    // Connect and parse the earthquake list
    private void httpConnection(URL url) {
        HttpURLConnection meConnection = null;
        try {
            meConnection = (HttpURLConnection) url.openConnection();
            meConnection.connect();
            BufferedReader buffer = new BufferedReader(new InputStreamReader((new BufferedInputStream(meConnection.getInputStream()))));
            String currLine;
            int count = 0;
            boolean firstLine = true;
            infoArray = new ArrayList<String>();
            while ((currLine = buffer.readLine()) != null) {
                if (firstLine == false) {
                    final String[] lineParse = currLine.split("\\|");
                    infoArray.add(lineParse[lineParse.length - 3] + "M " + lineParse[lineParse.length - 1]);

                    ArrayList<String> infoArrayStore = new ArrayList<String>() {{
                            add(lineParse[2]);
                            add(lineParse[3]);
                            add(lineParse[lineParse.length - 3]);
                            add(lineParse[lineParse.length - 1]);
                        }};
                    Earthquakes.put(count, infoArrayStore);
                    count++;
                } else {firstLine = false;}
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (meConnection != null) {
                meConnection.disconnect();
            }
        }
    }

    // Send Intent upon new earthquake
    private void setEarthquakeCounter() {
        int currEarthquakeCount = infoArray.size();
        if (currEarthquakeCount > earthquakeCounter && earthquakeCounter != 0) {
            earthquakeCounter = currEarthquakeCount;
            ArrayList<String> earthquakeData = Earthquakes.get(0);
            Intent maps = new Intent(getApplicationContext(), MapActivity.class);
            maps.putExtra("New Earthquake", earthquakeData);
            startActivityForResult(maps, 1);
        }
        earthquakeCounter = currEarthquakeCount;
    }

    // Render in XML
    private void setListView() {
        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter((new ArrayAdapter<String>(this, R.layout.list_item, infoArray)));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent mapsIntent = new Intent(getApplicationContext(), MapActivity.class);
                mapsIntent.putExtra("Latlng", Earthquakes.get(position));
                startActivityForResult(mapsIntent, 1);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
