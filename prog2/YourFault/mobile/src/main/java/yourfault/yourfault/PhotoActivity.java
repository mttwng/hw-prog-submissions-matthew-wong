package yourfault.yourfault;

import android.view.Window;
import android.graphics.Bitmap;
import android.app.Activity;
import android.widget.ImageView;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.net.URL;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class PhotoActivity extends Activity {
    private static final String FlickrApiKey = "e52c31288b7671f3eb00d06bce13edeb";
    private static final String TAG = "________FLICKR DEBUG: ";

    public double lat;
    public double lng;
    public ImageView flickrImage;
    private int maxPhotos = 3;
    private HttpURLConnection meConnection = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_photo);

        lat = getIntent().getDoubleExtra("lat", 0.0);
        lng = getIntent().getDoubleExtra("lng", 0.0);
        flickrImage = (ImageView) findViewById(R.id.flickrImageView);
        httpConnection(Double.toString(lat),Double.toString(lng));

    }

    public void httpConnection (String lat, String lng){
        URL url;
        try {
            url = new URL("https://api.flickr.com/services/rest/?method=flickr.photos.search" + "&format=json" + "&nojsoncallback=1" + "&api_key=" + FlickrApiKey + "&lat=" + lat + "&lon=" + lng);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }
        try {
            meConnection = (HttpURLConnection) url.openConnection();
            meConnection.connect();
            BufferedReader buffer = new BufferedReader(new InputStreamReader((new BufferedInputStream(meConnection.getInputStream()))));
            String currLine;
            StringBuilder jsonStringBuilder = new StringBuilder();
            while ((currLine = buffer.readLine()) != null) {
                jsonStringBuilder.append(currLine);
            }
            ArrayList<URL> urlArray = new ArrayList<URL>();
            for (int i = 0; i < maxPhotos; i++) {
                String stringURL= parseJSON(jsonStringBuilder.toString(), i);
                if (stringURL == null) {
                    break;
                }
                urlArray.add((new URL(stringURL)));
            }
            setBitmap(urlArray);

        } catch (IOException e) {
            e.printStackTrace();
            return ;
        } finally {
            if (meConnection != null) {
                meConnection.disconnect();
            }
        }
    }

    private String parseJSON(String json, int counter) {
        String url;
        try {
            JSONObject jObject = new JSONObject(json);
            JSONObject photo = jObject.getJSONObject("photos").getJSONArray("photo").getJSONObject(counter);
            url = "https://farm" + photo.getString("farm") + ".staticflickr.com/" + photo.getString("server") + "/" + photo.getString("id") + "_" + photo.getString("secret") + ".jpg";
        } catch (JSONException e) {
            e.printStackTrace();
            return null;

        }
        return url;
    }

    private void setBitmap(ArrayList<URL> urlArray) {
        try {
            Thread.sleep(10000);
            flickrImage.setImageBitmap(convertBitmap(urlArray.get(0)));
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }

    private Bitmap convertBitmap(URL imageURL){
        Bitmap bitmapImage;
        HttpURLConnection newConnection = null;
        try {
            newConnection = (HttpURLConnection) imageURL.openConnection();
            newConnection.connect();
            bitmapImage = BitmapFactory.decodeStream(new BufferedInputStream(newConnection.getInputStream()));
        } catch (Exception  e) {
            e.printStackTrace();
            return null;
        } finally {
            if (newConnection != null) {
                newConnection.disconnect();
            }
        }
        return bitmapImage;

    }

}
