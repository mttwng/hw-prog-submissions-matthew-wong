package yourfault.yourfault;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MyStubBroadcastActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService((new Intent(this, ShakeSensorIntentService.class)));
        Intent i = new Intent();
        i.setAction("yourfault.yourfault.SHOW_NOTIFICATION");
        i.putExtra(MyPostNotificationReceiver.CONTENT_KEY, getString(R.string.title));
        sendBroadcast(i);
        finish();
    }
}
