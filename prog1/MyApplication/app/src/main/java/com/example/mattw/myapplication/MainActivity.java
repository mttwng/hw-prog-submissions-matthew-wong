package com.example.mattw.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final HashMap<String, Double> agemap = new HashMap<String, Double>();
        agemap.put("Human", 1.0);
        agemap.put("Bear (Oski)", 2.0);
        agemap.put("Cat", 3.2);
        agemap.put("Chicken", 5.33);
        agemap.put("Cow", 3.64);
        agemap.put("Deer", 2.29);
        agemap.put("Dog", 3.64);
        agemap.put("Duck", 4.21);
        agemap.put("Elephant", 1.14);
        agemap.put("Fox", 5.71);
        agemap.put("Goat", 5.33);
        agemap.put("Groundhog", 5.71);
        agemap.put("Guinea pig", 10.0);
        agemap.put("Hamster", 20.0);
        agemap.put("Hippopotamus", 1.78);
        agemap.put("Horse", 2.0);
        agemap.put("Kangaroo", 8.89);
        agemap.put("Lion", 2.29);
        agemap.put("Monkey", 3.2);
        agemap.put("Mouse", 20.0);
        agemap.put("Parakeet", 4.44);
        agemap.put("Pig", 3.2);
        agemap.put("Pigeon", 7.27);
        agemap.put("Rabbit", 8.89);
        agemap.put("Rat", 26.67);
        agemap.put("Sheep", 5.33);
        agemap.put("Squirrel", 5.0);
        agemap.put("Wolf", 4.44);

        final TextView outputage = (TextView) findViewById(R.id.textView2);
        Button convertCLick = (Button) findViewById(R.id.convert_button);
        convertCLick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String yourAnimal = ((Spinner) findViewById(R.id.input_spinner)).getSelectedItem().toString();
                String desiredAnimal = ((Spinner) findViewById(R.id.output_spinner)).getSelectedItem().toString();
                int inputAge;
                try {
                    inputAge = Integer.parseInt(((EditText) findViewById(R.id.editText)).getText().toString());
                } catch (Exception e) {
                    inputAge = 0;
                }
                Double outputAge = inputAge / agemap.get(yourAnimal) * agemap.get(desiredAnimal);
                Integer years = (int) Math.floor(outputAge);
                Double frac = outputAge - years;
                Integer days = (int) Math.floor(frac * 365);
                String pluralYears = " years, ";
                if (years == 1) {
                    pluralYears = " year, ";
                }
                String pluralDays = " days";
                if (days == 1) {
                    pluralDays = " day";
                }
                outputage.setText(years + pluralYears + days + pluralDays);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
